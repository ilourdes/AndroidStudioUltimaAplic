package curso.umg.gt.miultimaplic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ListasActivity extends AppCompatActivity {
    private RadioButton rdb1, rdb2,rdb3,rdb4,rdb5,rdb6;
    private Button enviar;
    private List<String> Listado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listas);
        Listado =new ArrayList<>();
        rdb1=(RadioButton) findViewById(R.id.rd1);
        rdb2=(RadioButton) findViewById(R.id.rd2);
        rdb3=(RadioButton) findViewById(R.id.rd3);
        rdb4=(RadioButton) findViewById(R.id.rd4);
        rdb5=(RadioButton) findViewById(R.id.rd5);
        rdb6=(RadioButton) findViewById(R.id.rd6);
    }

    public void enviar(View view){

        Toast dos=Toast.makeText(this,"listas de interes :"+Listado.toString(), Toast.LENGTH_LONG);
        dos.show();;
    }

    public void onRadioButtonClicked(View view){
        boolean check=((RadioButton)view).isChecked();

        switch (view.getId()){
            case R.id.rd1:
                if (check)
                    Listado.add("Android");
                break;
            case R.id.rd3:
                if (check)
                    Listado.add("Java");
                break;
            case R.id.rd5:
                if (check)
                    Listado.add("Spring");
                break;
        }
    }

    public void onRadioButtonClickedRemove(View view){
        boolean check=((RadioButton)view).isChecked();

        switch (view.getId()){
            case R.id.rd2:
                if (check)
                    Listado.remove("Android");
                break;
            case R.id.rd4:
                if (check)
                    Listado.remove("Java");
                break;
            case R.id.rd6:
                if (check)
                    Listado.remove("Spring");
                break;
        }
    }



    }

